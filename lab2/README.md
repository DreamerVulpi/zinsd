## Сбор и аналитическая обработка информации о сетевом трафике

## Цель работы
1. Развить практические навыки использования современного стека инструментов сбора и аналитической обработки информации о сетевом трафике
2. Освоить базовые подходы блокировки нежелательного сетевого трафика
3. Закрепить знания о современных сетевых протоколах прикладного уровня

## Исходные данные

1. Персональный компьютер с установленной ОС Windows 10
2. Виртуальная машина Virtual Box
3. Образ Kali linux
4. Утилита Zeek
5. Программа Wireshark
6. Язык программирования Python
7. Среда разработки PyCharm Professional

## Варианты решения задачи

1. Собрать сетевой трафик (объёмом не менее 100 Мб)
2. Выделить метаинформацию сетевого трафика с помощью утилиты `Zeek`
3. Собрать данные об источниках нежелательного трафика из общедоступных источников, в частности использовать информацию из репозитория - https://github.com/StevenBlack
4. Написать программу на любом удобном языке (python, bash, R ...), которая подсчитывает процент нежелательного трафика. То есть подсчитать число вхождений DNS имён из списков в собранном трафике.
5. Оформить отчёт в соответствии с шаблоном - https://github.com/i2z1/Report_template


## Общий план выполнения

1. Используя образ `ОС Kali Linux` выполнить необходимые действия.

2. Написать программу, которая подсчитывает процент нежелательного трафика.


## Содержание лабораторной работы

### Шаг 1

Открываем `Wireshark`. Для захвата пакетов выбираем интерфейс `eth0` и запускаем программу нажав на кнопку с синим плавником слева сверху.

![image](step1.png)

### Шаг 2
Во время работы программы начинаем сёрфить по интернету: заходить на разные сайты, прожимать рекламные баннеры и т.д для сбора необходимой информации для дальнейшего выполнения лабораторной работы.

![image](step2.png)

### Шаг 3

После некоторого времени приостанавливаем работу программы и сохраняем полученные результаты. В данном случае, результаты будут сохранены в файл `results_surfing_internet.pcapng` (файл весит 233.3 MB)

![image](step3.png)

### Шаг 4

Выделяем метаинформацию сетевого трафика с помощью утилиты `Zeek`. Для этого применим необходимую команду: `zeek -C -r results_surfing_internet.pcapng` После выполнения получим файлы логи. Нас интересует `dns.log` и именно его будет использовать написанная программа для дальнейшей работы.

![image](step4.png)

### Шаг 5

Пользуясь источником https://github.com/StevenBlack необходимые данные о нежелательных DNS именах сохраняем в файл `BadHosts`. 

### Шаг 6

Напишем программу на Python, которая подсчитывает процент нежелательного трафика.

```py
import csv
import re
import matplotlib.pyplot as plt


def graphic_model(counter_hosts, counter_bad_hosts):
    labels = "Normal hosts", "Bad hosts"
    sizes = [counter_hosts - counter_bad_hosts, counter_bad_hosts]
    colors = ["#1F77B4", "#E43D40"]
    fig1, ax1 = plt.subplots()
    ax1.pie(sizes,  colors=colors, labels=labels, autopct='%1.1f%%', shadow=True, startangle=90)
    ax1.axis('equal')
    plt.show()


def percent_negative_connections(dns_filename):
    dns = open(dns_filename)
    result_surfing = csv.reader(dns, delimiter="\x09")
    file_with_bad_hosts = open('badHosts')
    list_bad_hosts = []

    for host in file_with_bad_hosts.readlines():
        if host != '\n':
            list_bad_hosts.append(re.compile('[^a-zA-Z.]').sub('', host))

    counter_bad_hosts = 0
    counter_hosts = 0
    for line in result_surfing:
        try:
            if line[9] in list_bad_hosts:
                counter_bad_hosts += 1
            counter_hosts += 1
        except IndexError:
            pass
    print(f"counter_hosts = {counter_hosts} counter_bad_hosts = {counter_bad_hosts}")
    print(f"Percent negative traffic = {round(100 * counter_bad_hosts / counter_hosts, 2)}%")
    graphic_model(counter_hosts, counter_bad_hosts)


if __name__ == '__main__':
    percent_negative_connections('dns.log')

```

## Оценка результата

Результатом для собранных данных в моём случае является `~29.3%` нежелательного графика

![image](step5.png)

![image](graphic.png)

## Вывод

Мной было изучено захватывать трафик при помощи программы `Wireshark` и разбор данных для анализа при помощи `Zeek`.

