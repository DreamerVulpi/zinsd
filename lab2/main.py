import csv
import re
import matplotlib.pyplot as plt


def graphic_model(counter_hosts, counter_bad_hosts):
    labels = "Normal hosts", "Bad hosts"
    sizes = [counter_hosts - counter_bad_hosts, counter_bad_hosts]
    colors = ["#1F77B4", "#E43D40"]
    fig1, ax1 = plt.subplots()
    ax1.pie(sizes,  colors=colors, labels=labels, autopct='%1.1f%%', shadow=True, startangle=90)
    ax1.axis('equal')
    plt.show()


def percent_negative_connections(dns_filename):
    dns = open(dns_filename)
    result_surfing = csv.reader(dns, delimiter="\x09")
    file_with_bad_hosts = open('badHosts')
    list_bad_hosts = []

    for host in file_with_bad_hosts.readlines():
        if host != '\n':
            list_bad_hosts.append(re.compile('[^a-zA-Z.]').sub('', host))

    counter_bad_hosts = 0
    counter_hosts = 0
    for line in result_surfing:
        try:
            if line[9] in list_bad_hosts:
                counter_bad_hosts += 1
            counter_hosts += 1
        except IndexError:
            pass
    print(f"counter_hosts = {counter_hosts} counter_bad_hosts = {counter_bad_hosts}")
    print(f"Percent negative traffic = {round(100 * counter_bad_hosts / counter_hosts, 2)}%")
    graphic_model(counter_hosts, counter_bad_hosts)


if __name__ == '__main__':
    percent_negative_connections('dns.log')
